# github-api-consumer

The GitHub API Consumer project allows listing all user repositories on the GitHub platform.


## Features
The project has only one endpoint:
- [ ] `GET /github/{username}`: The main and only functionality of the application. The endpoint returns a list of all 
- user repositories along with a list of all branches and the latest commit on each branch.


## Error Handling
The application handles the following errors:
- [ ] Error 404: If the user is not found.
- [ ] Error 406: If the "Accept" header is set to "application/xml". The application only supports JSON format.


## Running the Application
To run the application, you need to meet the following requirements:
***Have Maven installed on your computer.***
Then, follow these steps:

1. Open your terminal.
2. Navigate to the project's root directory containing the pom.xml file.
3. Launch the application using the Maven command: `mvn spring-boot:run -Dgithub.api.access-token=YOUR_ACCESS_TOKEN`
   The access token will be provided by the project administrator upon prior contact.
4. The application will run on http://localhost:8080
