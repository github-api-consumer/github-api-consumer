package pl.grzegorz.githubconsumer.views;

import lombok.AccessLevel;
import lombok.Builder;
import pl.grzegorz.githubconsumer.dtos.BranchDto;

@Builder(access = AccessLevel.PRIVATE)
public record BranchView(
        String name,
        String lastCommitSha
) {

    public static BranchView toView(final BranchDto branchDto) {
        return BranchView.builder()
                .lastCommitSha(branchDto.commit().sha())
                .name(branchDto.name())
                .build();
    }
}