package pl.grzegorz.githubconsumer.views;

import lombok.AccessLevel;
import lombok.Builder;
import pl.grzegorz.githubconsumer.dtos.RepositoryDto;

import java.util.List;

@Builder(access = AccessLevel.PRIVATE)
public record RepositoryView(
        String repositoryName,
        String ownerLogin,
        List<BranchView> branches
) {

    public static RepositoryView toView(final RepositoryDto repositoryDto, final List<BranchView> branches) {
        return RepositoryView.builder()
                .repositoryName(repositoryDto.name())
                .ownerLogin(repositoryDto.owner().login())
                .branches(branches)
                .build();
    }
}