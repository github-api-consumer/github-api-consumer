package pl.grzegorz.githubconsumer.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.githubconsumer.exceptions.ExceptionMessage;
import pl.grzegorz.githubconsumer.services.GithubService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/github")
class GithubController {

    private final GithubService githubService;

    @GetMapping("/{username}")
    ResponseEntity<?> getDetailsOfAllRepositories(final @PathVariable("username") String username) {
        if (!githubService.verifyAcceptHeader()) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .header("Content-Type", "application/json")
                    .body(ExceptionMessage.ACCEPT_HEADER_EXCEPTION_MESSAGE.getMessage());
        }
        return ResponseEntity.ok().body(githubService.getAllGithubRepositoriesByUsername(username));
    }
}