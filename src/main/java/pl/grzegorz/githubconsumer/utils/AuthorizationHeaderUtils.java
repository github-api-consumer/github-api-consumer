package pl.grzegorz.githubconsumer.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AuthorizationHeaderUtils {

    @Value("${github.api.access-token}")
    private static String accessToken;

    public static void setAccessTokenAuthorizationHeader(final HttpHeaders headers) {
        headers.set("Authorization", String.format("Bearer %s", accessToken));
    }
}