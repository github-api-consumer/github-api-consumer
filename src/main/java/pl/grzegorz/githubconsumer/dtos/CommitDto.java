package pl.grzegorz.githubconsumer.dtos;

public record CommitDto(
        String sha
) {
}