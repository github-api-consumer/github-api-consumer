package pl.grzegorz.githubconsumer.dtos;

public record OwnerDto(
        String login
) {
}