package pl.grzegorz.githubconsumer.dtos;

public record BranchDto(
        String name,
        CommitDto commit
) {
}