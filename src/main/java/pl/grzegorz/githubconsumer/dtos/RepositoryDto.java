package pl.grzegorz.githubconsumer.dtos;

public record RepositoryDto(
        String name,
        OwnerDto owner
) {
}