package pl.grzegorz.githubconsumer.services;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import pl.grzegorz.githubconsumer.dtos.BranchDto;
import pl.grzegorz.githubconsumer.dtos.RepositoryDto;
import pl.grzegorz.githubconsumer.exceptions.ExceptionMessage;
import pl.grzegorz.githubconsumer.exceptions.UserNotFoundException;
import pl.grzegorz.githubconsumer.utils.AuthorizationHeaderUtils;
import pl.grzegorz.githubconsumer.views.BranchView;
import pl.grzegorz.githubconsumer.views.RepositoryView;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
class GithubServiceImpl implements GithubService {

    private static final String REPO_LIST_API_URL = "https://api.github.com/users/%s/repos";
    private static final String COMMIT_LIST_API_URL = "https://api.github.com/repos/%s/%s/branches";

    private final RestTemplate restTemplate;
    private final HttpServletRequest httpServletRequest;
    private final HttpHeaders httpHeaders;

    @Override
    public List<RepositoryView> getAllGithubRepositoriesByUsername(final String username) {
        AuthorizationHeaderUtils.setAccessTokenAuthorizationHeader(httpHeaders);
        try {
            return fetchRepositoriesFromGithubByUsername(username)
                    .stream()
                    .map(repository -> {
                        List<BranchView> branches = fetchAndMapGithubBranchesToBranchView(repository);
                        return RepositoryView.toView(repository, branches);
                    })
                    .collect(Collectors.toList());
        } catch (HttpClientErrorException.NotFound e) {
            log.error("User using username -> {} not found", username);
            throw new UserNotFoundException(String.format(
                    ExceptionMessage.USER_NOT_FOUND_EXCEPTION_MESSAGE.getMessage(), username));
        }
    }

    @Override
    public boolean verifyAcceptHeader() {
        return !httpServletRequest.getHeader("accept").equals("application/xml");
    }

    private List<RepositoryDto> fetchRepositoriesFromGithubByUsername(final String username) {
        return List.of(Objects.requireNonNull(restTemplate.getForObject(String.format(REPO_LIST_API_URL, username),
                RepositoryDto[].class)));
    }

    private List<BranchView> fetchAndMapGithubBranchesToBranchView(final RepositoryDto repository) {
        return Arrays
                .stream(Objects.requireNonNull(restTemplate.getForObject(String.format(COMMIT_LIST_API_URL,
                        repository.owner().login(), repository.name()), BranchDto[].class)))
                .map(BranchView::toView)
                .toList();
    }
}