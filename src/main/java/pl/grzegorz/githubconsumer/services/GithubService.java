package pl.grzegorz.githubconsumer.services;

import pl.grzegorz.githubconsumer.views.RepositoryView;

import java.util.List;

public interface GithubService {

    List<RepositoryView> getAllGithubRepositoriesByUsername(String githubUserName);

    boolean verifyAcceptHeader();
}