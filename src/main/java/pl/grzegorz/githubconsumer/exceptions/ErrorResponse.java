package pl.grzegorz.githubconsumer.exceptions;

import lombok.AccessLevel;
import lombok.Builder;

@Builder(access = AccessLevel.PRIVATE)
record ErrorResponse(
        int status,
        String message
) {

    static ErrorResponse toErrorResponse(final int status, final String message) {
        return ErrorResponse.builder()
                .status(status)
                .message(message)
                .build();
    }
}