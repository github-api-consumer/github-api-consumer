package pl.grzegorz.githubconsumer.exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ExceptionMessage {

    USER_NOT_FOUND_EXCEPTION_MESSAGE("User using username [%s] not found"),
    ACCEPT_HEADER_EXCEPTION_MESSAGE("{\"status\": 406, \"message\": \"Accept header value 'application/xml' is wrong. " +
            "You need to change the accept header to 'application/json'\"}");

    private final String message;
}